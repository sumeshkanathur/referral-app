
## Prerequisites

Before you begin, make sure you have the following installed:

XAMPP/LAMP/WAMP server
PHP >= 7.2.0
Composer

## Installation

1. Clone the Laravel repository to your local machine using the following command:
git clone https://github.com/referral-app/laravel.git

2. Change your working directory to the Laravel repository
 cd referral-app

3. Install the dependencies using Composer:
composer install
 
4. Create a copy of the .env.example file and rename it to .env:
cp .env.example .env

5. run migration using command
php artisan migrate

6. run the command for configure route cache
php artisan route cache

7. Open the XAMPP/LAMP/WAMP control panel and start the Apache and MySQL services.

8. Open your web browser and go to http://localhost/referral-app/public to access the Laravel application.

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

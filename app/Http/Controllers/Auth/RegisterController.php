<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    public function index()
    {
        return view('index');
    }
    public function register(Request $request)
    {

        $validatedData = $request->validate([
            'name' => 'required|string',
            'email' => 'required|email|unique:users'
        ]);
        $referedBy = User::where('referal_code', $request->referred_by)->first();
        $user = new User();
        $user->name = $validatedData['name'];
        $user->email = $validatedData['email'];
        $user->referal_code = Str::random(10);
        $user->refered_by = $referedBy ? $referedBy->id : null;
        $user->level = $referedBy ? $referedBy->level + 1 : 0;

        $points = [10, 9, 8, 7, 6, 5, 4, 3, 2, 0];
        $user->points = $points[$user->level];
        $user->save();
        //Add points to the referring user
        if ($user->refered_by) {
            $referedBy->points += $user->points;
            $referedBy->save();
        }
        return redirect()->route('index')->with('success', 'User Registered Successfully');
    }

    public function list()
    {
        $users = User::all();
        return view('list', compact('users'));
    }
}
